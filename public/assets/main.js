// Webpack Imports

(function ($) {
  "use strict";

  // Focus input if Searchform is empty
  $(".search-form").on("submit", function (e) {
    var search = $(this).find("input");
    if (search.val().length < 1) {
      e.preventDefault();
      search.trigger("focus");
    }
  });
})(jQuery);

// header navigation
const navMenu = document.getElementById("nav-menu");
toggleMenu = document.getElementById("toggle-menu");
closeMenu = document.getElementById("close-menu");

toggleMenu.addEventListener("click", () => {
  navMenu.classList.toggle("show");
});
closeMenu.addEventListener("click", () => {
  navMenu.classList.remove("show");
});

// slider section 2
$(document).ready(function () {
  $(".wrapper").slick({
    dots: false,
    infinite: false,
    slidesToShow: 3,
    prevArrow: `<button type="button" class="slick-prev d-none">Previous</button>`,
    nextArrow: `<button type="button" class="slick-next d-none">Next</button>`,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          dots: true,
          slidesToShow: 2,
          breakpoint: 991,
        },
      },
    ],
  });
});

// slider section 5
$(document).ready(function () {
  $(".image-slider").slick({
    dots: true,
    prevArrow: `<button type="button" class="slick-prev slick-arrow shadow-sm"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-left mb-1" viewBox="0 0 16 16">
    <path d="M10 12.796V3.204L4.519 8 10 12.796zm-.659.753-5.48-4.796a1 1 0 0 1 0-1.506l5.48-4.796A1 1 0 0 1 11 3.204v9.592a1 1 0 0 1-1.659.753z"/>
  </svg></button>`,
    nextArrow: `<button type="button" class="slick-next slick-arrow shadow-sm"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-right mb-1" viewBox="0 0 16 16">
    <path d="M6 12.796V3.204L11.481 8 6 12.796zm.659.753 5.48-4.796a1 1 0 0 0 0-1.506L6.66 2.451C6.011 1.885 5 2.345 5 3.204v9.592a1 1 0 0 0 1.659.753z"/>
  </svg></button>`,
  });
});

// slider section 8
$(document).ready(function () {
  $(".social-icon").slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 6,
    prevArrow: `<button type="button" class="slick-prev d-none">Previous</button>`,
    nextArrow: `<button type="button" class="slick-next d-none">Next</button>`,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 5,
          dots: true,
        },
      },
      {
        breakpoint: 776,
        settings: {
          slidesToShow: 4,
          dots: true,
        },
      },
    ],
  });
});

// section 5 v2
$(document).ready(function () {
  $(".logo-image").slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 6,
    prevArrow: `<button type="button" class="slick-prev d-none">Previous</button>`,
    nextArrow: `<button type="button" class="slick-next d-none">Next</button>`,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 5,
          dots: true,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 3,
          dots: true,
        },
      },
      {
        breakpoint: 499,
        settings: {
          slidesToShow: 2,
          dots: true,
        },
      },
    ],
  });
});

// section 7 v2
$(document).ready(function () {
  $(".socials").slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 6,
    slidetoScroll: 7,
    prevArrow: `<button type="button" class="slick-prev d-none">Previous</button>`,
    nextArrow: `<button type="button" class="slick-next d-none">Next</button>`,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 4,
          dots: true,
        },
      },
      {
        breakpoint: 776,
        settings: {
          slidesToShow: 2,
          dots: true,
        },
      },
    ],
  });
});
